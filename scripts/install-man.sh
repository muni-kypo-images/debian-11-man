sudo apt-get update

# install chrony
sudo apt-get install -y chrony

# install guacd dependencies
sudo apt-get install -y make gcc g++ libcairo2-dev libjpeg62-turbo-dev libpng-dev libtool-bin uuid-dev libossp-uuid-dev libavcodec-dev libavutil-dev libswscale-dev libpango1.0-dev libssh2-1-dev libvncserver-dev libtelnet-dev libssl-dev libvorbis-dev libwebp-dev libpulse-dev libwebsockets-dev freerdp2-dev

# install guacd server
sudo apt-get install guacd -y
sudo systemctl start guacd
sudo systemctl enable guacd
