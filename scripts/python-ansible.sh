#!/bin/bash -x

# Install pip3
sudo apt-get install python3-pip -y
# Install ansible
sudo pip3 install ansible --no-input

# Install more modules
sudo pip3 install asn1crypto blinker chardet configobj "idna<3" jsonpatch jsonpointer jsonschema jwt oauthlib python-debian python-debianbts requests six urllib3 --no-input
