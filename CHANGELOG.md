# Changelog

## [qemu-0.1.0] - 2023-03-21
### Changed
- Initial setup - debian 11.6.0

[qemu-0.1.0]: https://gitlab.ics.muni.cz/muni-kypo-images/debian-11-man/-/tree/qemu-0.1.0
